function getBaseUrl(){

    return 'http://mdw.neolifeafrica.info:3000/posinvoice';
    //return 'http://localhost:3000/posinvoice';
}

function getParameterValueFromURL(url = '', parameter = ''){

    let query = url.split('?')[1];

    return new URLSearchParams(query).get(parameter);       
}

function getPrintButton(orderid = 0, extraCssClass = ''){

    return `<a href="${getBaseUrl()}/${orderid}" id="print-pos" target="_blank" class="btn ${extraCssClass} btn-warning"><i class="fa fa-print"></i> POS</a>`;
}
chrome.runtime.sendMessage({action : 'init'});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){

    if(request.action == 'addButton'){

        let orderid = getParameterValueFromURL(request.url, 'orderid');

        if(parseInt(orderid) < 1){
            
            chrome.runtime.sendMessage({action : 'notification', title : 'Order ID Not Found', message : 'Exigo Order ID cannot be found on this page.'});
        
        }else{

            $buttons = $('#buttons-container');
            if($('#print-pos').length < 1){

                $buttons.append(getPrintButton(orderid));
            }
            
        }
        
    }

    if(request.action == 'removeAutoPrintFromURL'){

        let $button = $('.btn.btn-info.fa-search');

        if($button.length < 1) return;

        $button.click(function(){
            
            let searchInterval = setInterval(function(){

                $invoicesTable = $('#invoicesTable');
    
                if($invoicesTable.length > 0){
                    
                    clearInterval(searchInterval);

                    $a = $invoicesTable.find('a').each(function(){
                        
                        $(this).attr('href', $(this).attr('href').replace('&autoPrint=true', ''));

                        let orderid = getParameterValueFromURL($(this).attr('href'), 'orderid');

                        if(parseInt(orderid) < 1){

                            return chrome.runtime.sendMessage({action : 'notification', title : 'Order ID Not Found', message : 'Exigo Order ID cannot be found on this page.'});
                        }

                        $(this).after(getPrintButton(orderid, 'btn-block'));

                    });
    
                }
    
            }, 1000);

        });

    }

    if(request.action == 'checkAutoPrintAndRedirect'){

        if(!request.url.includes('autoPrint=true')) return;
        let orderid = getParameterValueFromURL(request.url, 'orderid');

        if(parseInt(orderid) < 1){

            return chrome.runtime.sendMessage({action : 'notification', title : 'Order ID Not Found', message : 'Exigo Order ID cannot be found on this page.'});
        }

        window.stop();
        window.location = `${getBaseUrl()}/${orderid}?redirect=true`;
    }
    
});


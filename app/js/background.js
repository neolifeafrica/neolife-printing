
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
	
	if(request.action == 'init'){
		
		chrome.tabs.query({active : true, currentWindow : true}, function(tabs){
			
			let tab = tabs[0];
			chrome.pageAction.show(tab.id);

			if(tab.url.includes('invoice')){
				
				chrome.tabs.sendMessage(tab.id, {action : 'removeAutoPrintFromURL', url : tab.url});
			}
			
			if(tab.url.includes('invoice/printpage')){
				
				chrome.tabs.sendMessage(tab.id, {action : 'addButton', url : tab.url});
				chrome.tabs.sendMessage(tab.id, {action : 'checkAutoPrintAndRedirect', url : tab.url});
			}

		});
	}

	if(request.action == 'notification'){

		let notification = {type : 'basic', iconUrl : 'app/images/128.png', title : request.title, message : request.message};
		chrome.notifications.create('notification', notification);
	}
});